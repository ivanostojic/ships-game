<?php


namespace App\Models;


use App\Exceptions\GameAlreadyOverException;
use App\Exceptions\WeatherDoesNotExistException;
use Illuminate\Support\Arr;

class Game
{
    /**
     * @var Ship[] $ships
     */
    private array $ships = [];
    private int $numberOfMoves = 0;
    private Ship $currentMove;
    private string $weather;
    private int $turns;
    public array $moves = [];
    private bool $finished = false;
    public ?Ship $winner = null;
    private array $destroyedShips = [];
    private string $winType;

    /**
     * Create game with specified number of ships and soldiers
     *
     * @param array $ships
     */
    public function __construct(array $ships)
    {
        foreach ($ships as $index => $numberOfSoldiers)
            $this->ships[] = new Ship($numberOfSoldiers, $index);

        $this->currentMove = $this->ships[0];
    }

    public function setWeather(string $weather = 'normal')
    {
        $this->isAlreadyOver();
        throw_unless(collect(['normal', 'foggy', 'stormy'])->contains($weather), WeatherDoesNotExistException::class);
        $this->weather = $weather;
    }

    public function weatherSpecialEffects()
    {
        switch ($this->weather) {
            case 'normal':
                break;
            case 'stormy':
                foreach ($this->ships as $ship)
                    $ship->updateHealth(-10);
                $this->moves[] = [
                    'special_effect_type' => 'stormy',
                    'special_effect' => '-10 health to all ships'
                ];
                break;
            case 'foggy':
                foreach ($this->ships as $ship)
                    $ship->updateDefense(-5);
                $this->moves[] = [
                    'special_effect_type' => 'foggy',
                    'special_effect' => '-5 defense to all ships'
                ];
                break;
        }
    }

    public function setTurns(int $turns = 10)
    {
        if ($turns < 1)
            abort(400, 'You must set positive number of turns.');
        $this->isAlreadyOver();
        $this->turns = $turns;
    }

    /**
     * A ship that is currently on move will attack victim and log the details
     * @param Ship $attacker
     * @param Ship $defender
     */
    private function battle(Ship $attacker, Ship $defender): void
    {
        $attack = $attacker->attack();
        $defender->receiveAttack($attack->damage);
        $this->moves[] = [
            'attacker' => $attacker->name,
            'defender' => $defender->name,
            'damage' => $attack->damage,
            'defender_remained_health' => $defender->getHealth(),
            'critical_attack' => $attack->isCriticalAttack,
            'move_number' => $this->numberOfMoves + 1
        ];
    }

    public function execute(): void
    {
        $totalMoves = $this->turns * count($this->ships);
        while ($this->numberOfMoves < $totalMoves && !$this->finished) {
            $this->battle($this->currentMove, $this->pickVictimShip());
            if (rand(1, 10) > 8)
                $this->weatherSpecialEffects();
            $this->checkforDestroyedShips();
            $this->setNextMove();
            $this->checkForWinner();
        }
        if (!$this->finished) {
            $this->finished = true;
            $this->winner = $this->getShipWithGreatestHealth();
            $this->winType = 'Highest health ship';
        }
    }

    public function getShipWithGreatestHealth(): Ship
    {
        return Ship::findShipWithGreatestHealth($this->ships);
    }

    /**
     * Pick random alive ship to attack
     * @return Ship
     */
    private function pickVictimShip(): Ship
    {
        $availableShipsToAttack = collect($this->ships)->filter(function ($ship) {
            return $ship !== $this->currentMove;
        })->values()->all();
        return $availableShipsToAttack[array_rand($availableShipsToAttack)];
    }

    public function completeLog(): array
    {
        return [
            'winner' => $this->winner,
            'win_type' => $this->winType,
            'destroyed_ships' => $this->destroyedShips,
            'turns' => $this->turns,
            'number_of_played_moves' => $this->numberOfMoves,
            'weather' => $this->weather,
            'moves' => $this->moves
        ];
    }

    private function isAlreadyOver()
    {
        throw_if($this->finished, GameAlreadyOverException::class);
    }

    private function setNextMove(): void
    {
        $this->numberOfMoves++;
        $shipIndex = $this->numberOfMoves % count($this->ships);
        $this->currentMove = $this->ships[$shipIndex];
    }

    /**
     * If any of the ships are destroyed, remove them from ships array and push it to destroyed ships array
     */
    public function checkforDestroyedShips()
    {
        foreach ($this->ships as $key => $ship) {
            if ($ship->getIsDestroyed()) {
                $this->destroyedShips[] = Arr::pull($this->ships, $key);
                $this->ships = array_values($this->ships);
            }
        }
    }

    /**
     * If there is only one ship alive set it as the winner
     * @return bool
     */
    private function checkForWinner(): bool
    {
        if (count($this->ships) === 1) {
            $this->winner = $this->ships[array_key_first($this->ships)];
            $this->finished = true;
            $this->winType = 'Last standing ship';
            return true;
        }

        return false;
    }

}
