<?php

namespace App\Models;

class Ship
{
    public string $name;
    protected int $numberOfSoldiers;
    protected float $health;
    protected float $defense;
    protected float $attackLowerLimit;
    protected float $attackUpperLimit;
    protected float $criticalAttackChance;
    private bool $destroyed = false;

    public function __construct(int $numberOfSoldiers, string $index, float $health = 100,
                                float $attackLowerLimit = 10, float $attackUpperLimit = 15, float $defense = 5, float $criticalAttackChance = 15)
    {
        $this->numberOfSoldiers = $numberOfSoldiers;
        $this->name = "Ship $index";
        $this->health = $health;
        $this->attackLowerLimit = $attackLowerLimit;
        $this->attackUpperLimit = $attackUpperLimit;
        $this->defense = $defense;
        $this->criticalAttackChance = $criticalAttackChance;
    }

    public function setName(int $shipNumber): void
    {
        $this->name = "Ship $shipNumber";
    }

    /**
     * Increase/decrease health value and mark ship as destroyed if it's health is below 0
     * @param float $value
     */
    public function updateHealth(float $value)
    {
        $this->health += $value;
        if ($this->getHealth() < 0)
            $this->destroyed = true;
    }

    /**
     * Calculate the damage value for attack considering the number of soldiers + lower and upper attack limit
     * @return float
     */
    protected function baseAttack(): float
    {
        $baseAttack = rand($this->attackLowerLimit, $this->attackUpperLimit);

        return $baseAttack + ($this->numberOfSoldiers * 0.01 * $baseAttack);
    }

    public function attack(): Attack
    {
        $isCriticalAttack = rand(0, 100) < $this->criticalAttackChance;
        if ($isCriticalAttack)
            return Attack::createFrom($this->criticalAttack(), true);

        return Attack::createFrom($this->baseAttack(), false);
    }

    protected function criticalAttack(): float
    {
        return $this->baseAttack() * 2;
    }

    /**
     * Mitigate the attacker's damage value given the value of defense and decrease health
     * @param float $damageValue
     */
    public function receiveAttack(float $damageValue): void
    {
        $damageValue = $damageValue - $this->defense;
        $this->updateHealth(-$damageValue);
    }

    public function getIsDestroyed(): bool
    {
        return $this->destroyed;
    }

    public function getHealth(): float
    {
        return $this->health;
    }

    public static function findShipWithGreatestHealth(array $ships): Ship
    {
        return collect($ships)->sortByDesc(function ($item) {
            return $item->health;
        })->first();
    }

    public function updateDefense(float $defenseValue)
    {
        $this->defense += $defenseValue;
    }
}
