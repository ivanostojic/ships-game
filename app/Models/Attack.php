<?php

namespace App\Models;

class Attack
{
    public float $damage;
    public bool $isCriticalAttack;

    public function __construct(float $damage, bool $isCriticalAttack)
    {
        $this->damage = $damage;
        $this->isCriticalAttack = $isCriticalAttack;
    }

    public static function createFrom(float $criticalAttack, bool $isCriticalAttack): Attack
    {
        return new static($criticalAttack, $isCriticalAttack);
    }
}
