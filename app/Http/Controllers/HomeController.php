<?php


namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Ship;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function newGame(Request $request)
    {
        $request->validate([
            'ships' => 'required|array',
            'ships.*' => 'required|integer',
            'weather' => 'nullable|in:normal,foggy,stormy',
            'turns' => 'nullable|integer'
        ]);

        $game = new Game($request->ships);
        $game->setTurns($request->turns ?? 10);
        $game->setWeather($request->weather ?? 'normal');
        $game->execute();
        return $game->completeLog();
    }
}
